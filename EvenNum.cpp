﻿
#include <iostream>
void PrintNumbers(int N, bool isOdd)
{

	for (int i = 0; i < N; i++)
	{
		int c = i % 2;
		if (c == 0 && isOdd)
		{ 
			std::cout << "Even Num: " << i << '\n';
		}
		else if (c != 0 && !isOdd)
		{
			std::cout << "Other Num: " << i << '\n';
		}
	}
}

int main()
{
	PrintNumbers(100, true);
	PrintNumbers(100, false);
	
}



//void PrintNumbers(int N, bool isOdd)
//{
//	
//	for (int i = 0; i < N; i++) 
//	{
//		int c =  i % 2;
//		if (c == 0) 
//		{
//			if (isOdd) 
//			{
//			std::cout << "Even Num: " << i << '\n';
//
//			}
//		}
//		else 
//		{
//			if (!isOdd) 
//			{
//			std::cout << "Other Num: " << i << '\n';
//
//			}
//		}
//	}
//}